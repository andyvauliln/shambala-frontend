const express = require('express');
const path = require('path');

const app = express();

app.get('/env.js', function(req, res) {
  res.set('Content-Type', 'application/javascript');
});

app.get('/health', function(req, res) {
  console.log('I norm');
  res.send('I norm');
});

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(process.env.PORT);
