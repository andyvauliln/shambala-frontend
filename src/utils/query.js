import axios from 'axios';
import buildUrl from 'build-url';

export function databaseEndpoint(withDatabase = false) {
  const url = {
    path: '/',
    queryParams: {},
  };

  const user = 'default';
  if (user) {
    url.queryParams.user = user;
  }

  const password = '';
  if (password) {
    url.queryParams.password = password;
  }

  if (withDatabase) {
    url.queryParams.database = 'production';
  }

  return buildUrl('https://oraclize.tk/clickhouse', url);
}

export async function runQuery(query, withDatabase = false) {
  return axios.post(databaseEndpoint(withDatabase), `${query} FORMAT JSON`);
}
